import hudson.security.csrf.DefaultCrumbIssuer
import jenkins.model.Jenkins

// 중단모드일 경우 시행하면 안됨
if(!Jenkins.instance.isQuietingDown()){

  //CSRF 설정이 있는 경우
  if(Jenkins.instance.getCrumbIssuer()!=null){
    def instance = Jenkins.instance
		// DefaultCrumbIssuer(false) : XFF헤더 정의
    instance.setCrumbIssuer(new DefaultCrumbIssuer(false))
    instance.save()

    println 'excludeClientIPfromCrumb set: false'

  }else{
	  println 'Nothing changed'
	}
}

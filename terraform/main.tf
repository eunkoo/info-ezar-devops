# aws_instance.Devops:
resource "aws_instance" "Devops" {
    ami                                  = "ami-0ed11f3863410c386"
    arn                                  = "arn:aws:ec2:ap-northeast-2:729097534527:instance/i-03c1b37759c3c59dc"
    associate_public_ip_address          = true
    availability_zone                    = "ap-northeast-2c"
    cpu_core_count                       = 2
    cpu_threads_per_core                 = 1
    disable_api_termination              = false
    ebs_optimized                        = false
    get_password_data                    = false
    hibernation                          = false
    id                                   = "i-03c1b37759c3c59dc"
    instance_initiated_shutdown_behavior = "stop"
    instance_state                       = "running"
    instance_type                        = "t2.medium"
    ipv6_address_count                   = 0
    ipv6_addresses                       = []
    key_name                             = "ezar-devops"
    monitoring                           = false
    primary_network_interface_id         = "eni-08b2049e262fb5103"
    private_dns                          = "ip-172-31-32-190.ap-northeast-2.compute.internal"
    private_ip                           = "172.31.32.190"
    public_dns                           = "ec2-3-34-45-133.ap-northeast-2.compute.amazonaws.com"
    public_ip                            = "3.34.45.133"
    secondary_private_ips                = []
    security_groups                      = [
        "launch-wizard-5",
    ]
    source_dest_check                    = true
    subnet_id                            = "subnet-037f6feadd8c40c5c"
    tags                                 = {
        "Name" = "Devops"
    }
    tags_all                             = {
        "Name" = "Devops"
    }
    tenancy                              = "default"
    vpc_security_group_ids               = [
        "sg-053450615ef121d40",
    ]

    capacity_reservation_specification {
        capacity_reservation_preference = "open"
    }

    credit_specification {
        cpu_credits = "standard"
    }

    enclave_options {
        enabled = false
    }

    metadata_options {
        http_endpoint               = "enabled"
        http_put_response_hop_limit = 1
        http_tokens                 = "optional"
        instance_metadata_tags      = "disabled"
    }

    root_block_device {
        delete_on_termination = true
        device_name           = "/dev/sda1"
        encrypted             = false
        iops                  = 100
        tags                  = {
            "Name" = "Devops EC2 Instance"
        }
        throughput            = 0
        volume_id             = "vol-062af6d365fbffbca"
        volume_size           = 30
        volume_type           = "gp2"
    }

    timeouts {}
}
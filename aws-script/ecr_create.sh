#!/bin/bash
aws ecr create-repository \
    --repository-name ${1} \
    --image-scanning-configuration scanOnPush=true \
    --region ap-northeast-2 
